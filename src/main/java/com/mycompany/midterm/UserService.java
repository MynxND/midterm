/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm;

import java.io.File;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TUFGaming
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();
    private static User currentUser = null;

    static {
        load();
    }

    //Create (C)
    public static boolean addItem(User item) {
        userList.add(item);
        save();
        return true;
    }

    //Delete (D)
    public static boolean delItem(User item) {
        userList.remove(item);
        save();
        return true;
    }

    public static boolean delItem(int index) {
        userList.remove(index);
        save();
        return true;
    }

    //Read (R)
    public static ArrayList<User> getItem() {
        return userList;
    }
    public static void clear(){
        userList.clear();
        save();
    }
    public static User getItem(int index) {
        return userList.get(index);
    }

    public static String overallPrice() {
        double price = 0;
        int amount = 0;
        for (int i = 0; i < userList.size(); i++) {
            price += userList.get(i).getTxtPrice();
            amount +=userList.get(i).getTxtAmount();
        }
        return ""+price+""+amount;
    }

    //User (U)
    public static boolean updateItem(int index, User user) {
        userList.set(index, user);
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("pop.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("pop.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
