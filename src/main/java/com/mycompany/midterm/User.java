package com.mycompany.midterm;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mynx
 */
 
public class User implements Serializable{

    private String txtID;
    private String txtName;
    private String txtBrand;
    private double txtPrice;
    private int txtAmount;


    public User(String txtID, String txtName, String txtBrand
    ,double txtPrice,int txtAmount) {
        this.txtID = txtID;
        this.txtName = txtName;
        this.txtBrand = txtBrand;
        this.txtPrice = txtPrice;
        this.txtAmount = txtAmount;
    }


    public String getTxtName() {
        return txtName;
    }

    public void setTxtName(String txtName) {
        this.txtName = txtName;
    }

    public String getTxtBrand() {
        return txtBrand;
    }

    public void setTxtBrand(String txtBrand) {
        this.txtBrand = txtBrand;
    }

    public double getTxtPrice() {
        return txtPrice;
    }

    public void setTxtPrice(double txtPrice) {
        this.txtPrice = txtPrice;
    }

    public int getTxtAmount() {
        return txtAmount;
    }

    public void setTxtAmount(int txtAmount) {
        this.txtAmount = txtAmount;
    }

    public String getId() {
        return txtID;
    }

    public void setID(String txtID) {
        this.txtID = txtID;
    }



    @Override
    public String toString() {
        return "ID " + txtID + ", Name " + txtName;
    }

}
